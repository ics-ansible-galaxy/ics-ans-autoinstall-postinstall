import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('autoinstall_postinstall')


def test_chrony_service(host):
    s = host.service("chronyd")
    assert s.is_running
    assert s.is_enabled
