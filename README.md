# ics-ans-autoinstall-postinstall

Playbook called at the firstboot stage after a PXE/kickstart based auto-install.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
